# -*- coding: utf-8 -*-
"""
Module that implements several linear operators. 

Created on Thu Jan 29 13:23:53 2015

@author: stamatis@math.ucla.edu
"""
import numpy as np

def shift(x,s,bc='circular'):
    """ Shift operator that can treat different boundary conditions. It applies 
    to an nd-array of arbitrary dimensions. 
    ----------
    Usage: xs=shift(x,[0,1,-3,3],'reflexive')
    ----------
    Parameters
    ----------
    x : nd-array.
    s : vector that matches the dimensions of x, with the corresponding shifts.
    bc: String with the prefered boundary conditions (bc='circular'|'reflexive','zero')
        (Default: 'circular')
    """
    
    x=np.asanyarray(x)
    
    # Make sure that s does not contain any inf or nan values
    if np.any(np.isinf(s)) or np.any(np.isnan(s)) or np.any(np.iscomplex(s)):
        raise Exception("The shift vector s should only consist of real integer numbers.")
    
    if not isinstance(bc,np.str):
        raise Exception("bc must be of type string")
       
    s=np.asanyarray(s,dtype=int)
    s=s.flatten() # treat s always as a vector and not an nd-array
    
    x_dims=x.ndim
    
    if s.size < x_dims:
        s=np.r_[s,np.zeros(x.ndim-s.size,dtype=int)]
    elif s.size > x_dims:
        print("The shift array will be truncated to match the " \
        +"dimensions of the input nd-array. The trailing extra elements will" \
        +" be discarded.")
        s=s[0:x_dims]
    
    if np.any(np.abs(s) > np.array(x.shape)):
        raise Exception("The shift steps should not exceed in absolute values"\
        +" the size of the corresponding dimensions.")
        
    # use the list sequence instead of the tuple since the latter is an 
    # immutable sequence and cannot be altered 
    indices=[slice(0,x.shape[0])]
    for i in range(1,x_dims):
        indices.append(slice(0,x.shape[i]))
        
    if bc=='circular':
        xs=x[:] # make a copy of x
        for i in range(x_dims):
            if s[i]==0:
                continue
            else:
                m=x.shape[i]
                idx=indices[:]                
                idx[i]=(np.arange(0,m)-s[i])%m
                xs=xs[idx]
    elif bc=='reflexive':
        xs=x[:] # make a copy of x
        for i in range(x_dims):
            if s[i]==0:
                continue
            else:
                idx=indices[:]
                if s[i]>0: # right shift
                    idx[i]=list(range(s[i]-1,-1,-1))+list(range(0,x.shape[i]-s[i]))
                else: # left shift
                    idx[i]=list(range(-s[i],x.shape[i]))+list(range(-1,s[i]-1,-1))       
                
                xs=xs[idx]
    elif bc=='zero':
        xs=np.zeros(x.shape,dtype=x.dtype)        
        idx_x=indices[:]
        idx_xs=indices[:]
        for i in range(x_dims):
            if s[i]==0:
                continue
            else:       
                if s[i]>0: # right shift
                    idx_x[i]=slice(0,x.shape[i]-s[i])
                    idx_xs[i]=slice(s[i],x.shape[i])
                else: # left shift
                    idx_x[i]=slice(-s[i],x.shape[i])
                    idx_xs[i]=slice(0,x.shape[i]+s[i])
        
        xs[idx_xs]=x[idx_x]
        
    else:
        raise Exception("Unknown boundary conditions")
    
    return xs


def Adjshift(x,s,bc='circular'):
        
    """ Adjoint of the shift operator that can treat different boundary conditions. 
    It applies to an nd-array of arbitrary dimensions. 
    ----------
    Usage: xs=Adjshift(x,[0,1,-3,3],'reflexive')
    ----------
    Parameters
    ----------
    x : nd-array.
    s : vector that matches the dimensions of x, with the corresponding shifts.
    bc: String with the prefered boundary conditions (bc='circular'|'reflexive','zero')
        (Default: 'circular')
    """   
    x=np.asanyarray(x)
    
    # Make sure that s does not contain any inf or nan values
    if np.any(np.isinf(s)) or np.any(np.isnan(s)) or np.any(np.iscomplex(s)):
        raise Exception("The shift vector s should only consist of real integer numbers.")
    
    if not isinstance(bc,np.str):
        raise Exception("bc must be of type string")
       
    s=np.asanyarray(s,dtype=int)
    s=s.flatten() # treat s always as a vector and not an nd-array
    
    x_dims=x.ndim
    
    if s.size < x_dims:
        s=np.r_[s,np.zeros(x.ndim-s.size,dtype=int)]
    elif s.size > x_dims:
        print("The shift array will be truncated to match the " \
        +"dimensions of the input nd-array. The trailing extra elements will" \
        +" be discarded.")
        s=s[0:x_dims]
    
    if np.any(np.abs(s) > np.array(x.shape)):
        raise Exception("The shift steps should not exceed in absolute values"\
        +" the size of the corresponding dimensions.")
        
    # use the list sequence instead of the tuple since the latter is an 
    # immutable sequence and cannot be altered 
    indices=[slice(0,x.shape[0])]
    for i in range(1,x_dims):
        indices.append(slice(0,x.shape[i]))
        
    if bc=='circular':
        xs=x[:] # make a copy of x
        for i in range(x_dims):
            if s[i]==0:
                continue
            else:
                m=x.shape[i]
                idx=indices[:]                
                idx[i]=(np.arange(0,m)+s[i])%m
                xs=xs[idx]
    elif bc=='reflexive':
        y=x[:]
        for i in range(x_dims):
            xs=np.zeros(x.shape,dtype=x.dtype)
            idx_x_a=indices[:]
            idx_x_b=indices[:]
            idx_xs_a=indices[:]
            idx_xs_b=indices[:]
            if s[i]==0:
                xs=y[:]
            else:
                if s[i] > 0:
                    idx_xs_a[i]=slice(0,-s[i])
                    idx_xs_b[i]=slice(0,s[i])
                    idx_x_a[i]=slice(s[i],None)
                    idx_x_b[i]=slice(s[i]-1,None,-1)
                else:
                    idx_xs_a[i]=slice(-s[i],None)
                    idx_xs_b[i]=slice(s[i],None)
                    idx_x_a[i]=slice(0,s[i])
                    idx_x_b[i]=slice(-1,s[i]-1,-1)
                
                xs[idx_xs_a]=y[idx_x_a]
                xs[idx_xs_b]=xs[idx_xs_b]+y[idx_x_b]
                y=xs[:]
        
    elif bc=='zero':
        xs=np.zeros(x.shape,dtype=x.dtype)        
        idx_x=indices[:]
        idx_xs=indices[:]
        for i in range(x_dims):
            if s[i]==0:
                continue
            else:       
#                s[i]=-s[i]
#                if s[i]>0: # right shift
#                    idx_x[i]=slice(0,x.shape[i]-s[i])
#                    idx_xs[i]=slice(s[i],x.shape[i])
#                else: # left shift
#                    idx_x[i]=slice(-s[i],x.shape[i])
#                    idx_xs[i]=slice(0,x.shape[i]+s[i])
                
                if s[i]<0: 
                    idx_x[i]=slice(0,x.shape[i]+s[i])
                    idx_xs[i]=slice(-s[i],x.shape[i])
                else: 
                    idx_x[i]=slice(s[i],x.shape[i])
                    idx_xs[i]=slice(0,x.shape[i]-s[i])
        
        xs[idx_xs]=x[idx_x]
        
    else:
        raise Exception("Unknown boundary conditions")
    
    return xs

def im_grad(f,bc='reflexive'):
    """Computes the gradient of the input image for specified image boundaries. 
    The input image can be multi-dimensional."""
    Df=np.zeros(f.shape+(2,),dtype=f.dtype)
    Df[...,0]=shift(f,[-1,0],bc)-f # partial derivative w.r.t to axis=0
    Df[...,1]=shift(f,[0,-1],bc)-f # partial derivative w.r.t to axis=1
    return Df

def im_ndiv(Df,bc='reflexive'):
    """ Computes the adjoint of the image gradient (negative divergence)."""
    
    ndiv=Adjshift(Df[...,0],[-1,0],bc)-Df[...,0]\
            +Adjshift(Df[...,1],[0,-1],bc)-Df[...,1]
    return ndiv
    
    


if __name__=="__main__":
    x=10*np.random.randint(0,100,(50,30,42,50))
    y=10*np.random.randint(0,100,(50,30,42,50))
   
    sh=np.random.randint(-29,29,4)
    xs=shift(x,sh,bc='reflexive')
    yAs=Adjshift(y,sh,bc='reflexive')
    e=np.dot(xs.flatten(),y.flatten())-np.dot(x.flatten(),yAs.flatten())
    print(e)
    if abs(e) < 1e-15:
        print("Error check done succesfully!")
    else:
        print("Something wrong is going on!")
    
    del x,y,sh,xs,yAs,e
        
        
